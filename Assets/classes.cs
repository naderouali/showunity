﻿using System.Collections.Generic;

[System.Serializable]
public class Asset
{
    public string url;
}

[System.Serializable]
public class Shape
{

    public string _id, type, fill, content,color, stroke , action;

    public float x, y, width, height, cornerRadius, strokeWidth, opacity, fontSize, textOffsetX, textOffsetY;

    public Asset src;
}

[System.Serializable]
public class Page
{
    public string name;

    public Asset thumbnail, background;

    public List<Shape> shapes;
}

[System.Serializable]
public class Project
{
    public string name, packagename, keystore, icon;

    public List<Page> pages;
}

[System.Serializable]
public class CustomizationResponse
{
    public bool success;
    public string message;
    public Project project;
}