﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Customizer : MonoBehaviour
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public string json;

    public CustomizationResponse cr;

    public List<GameObject> shapes;

    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/


    /**********************************************/
    /*              PRIVATE VARIABLES             */
    /**********************************************/



    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    void Start()
    {
        cr = JsonUtility.FromJson<CustomizationResponse>(json);

        int sceneIndex = SceneManager.GetActiveScene().buildIndex;

        Page currentPage = cr.project.pages[sceneIndex];

        for (int i = 0; i < shapes.Count; i++)
        {
            GameObject g = shapes[i];
            Shape s = currentPage.shapes[i];


            g.GetComponent<Image>().color = toColor(s.fill, s.opacity);

            g.GetComponentInChildren<TextMeshProUGUI>().text = s.content;
            g.GetComponentInChildren<TextMeshProUGUI>().fontSize = s.fontSize * 4.3f;

            Vector2 referenceSize = FindObjectOfType<Canvas>().GetComponent<RectTransform>().sizeDelta;

            float width = s.width * referenceSize.x / 234;
            float height = s.height * referenceSize.y / 488;

            g.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);

            g.GetComponent<RectTransform>().anchoredPosition = convertPosition(s.x, s.y , width, height , referenceSize);

        }

    }

    /**********************************************/
    /*                   LOOP                     */
    /**********************************************/

    void Update()
    {

    }

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public Vector2 convertPosition(float x, float y , float width , float height , Vector2 referenceSize)
    {

        var tempX = referenceSize.x * (x / 234 - .5f) + width / 2;
        var tempY = referenceSize.y * (-y / 488 + .5f) - height / 2;

        return new Vector2(tempX , tempY);
    }

    public Color toColor(string hex, float opacity = 1)
    {
        Color color;



        if (ColorUtility.TryParseHtmlString(hex, out color))
        {

            color.a = opacity;

            return color;
        }
        else
        {
            return new Color(1, 1, 1, 0);
        }
    }

}
