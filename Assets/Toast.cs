﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toast : MonoBehaviour
{
    public GameObject textToast;


    public void disableToast()
    {
        Invoke("disableAfterAWhile", 1);
    }

    public void disableAfterAWhile()
    {
        textToast.SetActive(false);
    }
}
